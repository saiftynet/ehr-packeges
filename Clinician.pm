package Clinician;
    @ISA = (Anything);

	sub new {
		my $class  = shift;
		my %params = @_;
		my @directKeys=qw/Name Email Bleep Phone Grade Team Specialty Logs/;
		
		my $self   = Anything->new(
		    'Creator'   =>$params{'Creator'},
		    'Type'      =>"CLIN",
		    );
		    
		foreach $key (@directKeys){
		    $self{$key}=$params{$key}
		}
		return bless($self, $class);
	}


1;