			Login=>{        # entry context when userID not defined
				Label       => "Login",
				Menu        => [qw/ForgotPW Register/],
				Context     => "Login  to Traumabase",
				Content     => 'Form:
								Enter Username and password to login or use Register to Sign Up
				                input:Username:UN
				                dropdown(Referer,Specialist,Ward,Theatre,Patient):Enter Role:ROLE
				                input:Password:PW',
				Action      => sub{ my $inRef=shift;
					                my $success = &main::login($inRef);
				                    if    ($$inRef{ROLE} eq "Referer")    {$$inRef{ACTION}="NewRef" }
				                    elsif ($$inRef{ROLE} eq "Specialist") {$$inRef{ACTION}="NewPat" };
					                },
			},
			ForgotPW=>{
				Label       => "Forgotten Password",
				Menu        => [qw/Login Register/],
				Context     => "Forgotten Password Form",
				Content     => 'Form
				                Enter username.  If username exists, then you will be emailed a password reset link
				                input:Username:UN
				                dropdown(Referer,Specialist,Ward,Theatre,Patient):Enter Role:ROLE',
				Action      => sub{ my $inRef=shift;
				                    $$inRef{ACTION}="Login";
					                },
			},
			Register=>{
				Label       => "Register",
				Menu        => [qw/Login ForgotPW/],
				Context     => "New User Registration Form",
				Content     => "Form
				                Enter details below to register to use the service.
				                Once approved you will be able to login
				                input:Enter fullname:FULLNAME
				                dropdown(Referer,Specialist,Ward,Theatre,Patient):Enter Role:ROLE
				                input:Telephone number:TELNO
				                input:email address:EMAIL
				                input:Username:UN
				                input:Password:PW
				                input:Confirm PW:CPW",
				Action      => sub{ my $inRef=shift;
					                $$inRef{OID}=$$inRef{UN};
				                    my $success = &main::makeRecord($inRef);
				                    if ($success) {$$inRef{ACTION}= "Login"}
				                    else {$$inRef{ACTION}= "Register"}
				                  },
			},
			NewRef=>{  # entry context for referrers  Subject is a referrer
				Label       => "Refer",
				Menu        => [qw/AllRefs MyRefs SearchRefs/],
				Context     => "New Referral Form",
				Content     => "Form
				                Enter data to make a new referral.
				                Please enter your extension if you wish a call back
				                A response is expected within a maximum of 15 minutes of referral
				                (please check your messages). Do not use if updating an
				                existing referral, (find your referral and edit).  Uploading images 
				                help if you have the capability.  Anomymise if possible
				                
				                input:*Patient ID:PID
				                input:*First Names:FNAME
				                input:*Surname:SNAME
				                input:DOB:DOB
				                input:Location:LOC
				                input:Region:REGION
				                input:*Diagnosis:DIAG
				                input:Extension:EXTNO
				                text:*Notes:NOTES",
				Action      => sub{},
			},
			AllRefs=>{
				Label       => "All Referrals",
				Menu        => [qw/NewRef MyRefs SearchRefs/],
				Context     => "List of All Referrals",
				Content     => "List
				                click on referral to edit referral. Clicking cancel will cancel referral
				                but the referral will remain visible. Delete will cancel referral and delete from list.
				                when accepted the referral will disappear.  Referrals with response will be bordered
				                with red, but the repsonse will also appear on the referrers messages
				                List
				                Titles:PID, FNAME, SNAME, DOB, DIAG, REFTIME, MESSAGES",
				Action      => sub{},
			},
			MyRefs=>{
				Label       => "My Referrals",
				Menu        => [qw/NewRef AllRefs SearchRefs/],
				Context     => "List of Patients I have Referred",
				Content     => "List
				                click on referral to edit referral. Clicking cancel will cancel referral
				                but the referral will remain visible. Delete will cancel referral and delete from list.
				                Accepted the referral will also be deleted from this list.  Referrals with response
				                will be bordered with red, but the repsonse will also appear on the referrers messages
				                List
				                Titles:PID, FNAME, SNAME, DOB, DIAG, REFTIME, STATUS, MESSAGES",
				Action      => sub{},
			},
			SearchRefs=>{
				Label       => "Search",
				Menu        =>  [qw/NewRef AllRefs MyRefs/],
				Context     => "Search all referrals",
				Content     =>  " Enter data to find referral.
				                Please enter your extension if you wish a call back
				                A response is expected within a maximum of 15 minutes of referral
				                (please check your messages). Do not use if updating an
				                existing referral, (find your referral and edit).  Uploading images 
				                help if you have the capability.  Anomymise if possible
				                Form
				                input:*Patient ID:PID
				                input:*First Names:FNAME
				                input:*Surname:SNAME
				                input:DOB:DOB",
				Action      => sub{},
			},
			EditRef=>{ #now referer is subject and the patient is the object
				Label       => "Edit Referral",
				Menu        => [qw/NewRef AllRefs MyRefs SearchRefs/],
				Context     => "PatData",
				Content     => "FormPrePopulate
				                 edit existing referral.  Uploading images 
				                help if you have the capability.  Anomymise if possible
				                input:*Diagnosis:DIAG
				                input:Extension:EXTNO
				                textarea:*Notes:NOTES",
				Action      => sub{},
			},
			EditPatient=>{ #now referer is subject and the patient is the object
				Label       => "Edit Patient",
				Menu        => [qw/NewRef AllRefs MyRefs SearchRefs/],
				Context     => "PatData",
				Content     => "FormPrePopulate
				                 Modify patient demographic data if data
				                previously not entered or entered in error
				                input:Patient ID:PID
				                input:First Names:FNAME
				                input:Surname:SNAME
				                input:DOB:DOB",
				Action      => sub{},
			},
			Profile=>{      #entry context for user management available when user clicks on his ID Top right corner any user
				Label       => "Profile",
				Menu        => [qw/Profile ChangePW Tasks Messages/],
				Context     => "UserData",
				Content     => "FormPrePopulate
				                Preamble:Update profile details below.
				                input:Telephone number:TELNO
				                input:email address:EMAIL",
				Action      => sub{},
			},
			ChangePW=>{
				Label       => "Login",
				Menu        => [qw/Profile Tasks Messages/],
				Context     => "Change Password",
				Content     => "Form
				                Preamble:Enter new password below
				                input:Current Password:OPW
				                input:New Password:PW
				                input:Confirm New Password:CPW",
				Action      => sub{},
			},
			MyTasks=>{
				Label       => "My Tasks",
				Menu        => [qw/Profile ChangePW Messages/],
				Context     => "View my tasks",
				Content     => "list of tasks",
				Action      => sub{},
			},
			AllTasks=>{
				Label       => "All Tasks",
				Menu        => [qw/Profile ChangePW Messages/],
				Context     => "View my tasks",
				Content     => "list of tasks",
				Action      => sub{},
			},
			Messages=>{
				Label       => "Message",
				Menu        => [qw/Profile ChangePW Tasks Messages/],
				Context     => "Messaging platform",
				Content     => "Message Tree",
				Action      => sub{},
			},
			NewPat=>{  # when userID is specialist, and Object is not defined
				Label       => "New Patient",
				Menu        => [qw/Referrals MyPats SearchPats WardLists TheatreLists/],
				Context     => "New Patients Data Entry Form",
				Content     => "Form
				                Enter data to add a new patient to the database.
				                Note you will not be able to do this for referals already
				                entered through this system.  Chhose the Referrals list
				                to view patients waiting to be accepted. and add notes to
				                thse as needed.
				                
				                input:*Patient ID:PID
				                input:*First Names:FNAME
				                input:*Surname:SNAME
				                input:DOB:DOB
				                input:Region:REGION
				                input:*Diagnosis:DIAG
				                input:Extension:EXTNO
				                text:*Notes:NOTES",
				Action      => sub{},
			},
			Referrals=>{
				Label      => "Referrals",
				Menu       =>[],
				Context    =>"",
				Content    =>"",
				Action     =>sub{},
			},
			Lists=>{
				Label      => "Lists",
				Menu       =>[],
				Context    =>"",
				Content    =>"",
				Action     =>sub{},
			},
			SearchPats=>{
				Label      => "Search Patients",
				Menu       =>[],
				Context    =>"",
				Content    =>"",
				Action     =>sub{},
			},
			WardLists=>{
				Label      => "Ward Lists",
				Menu       =>[],
				Context    =>"",
				Content    =>"",
				Action     =>sub{},
			},
			TheatreLists=>{
				Label      => "Theatre Lists",
				Menu       =>[],
				Context    =>"",
				Content    =>"",
				Action     =>sub{},
			},
			MeetingLists=>{
				Label      => "Theatre Lists",
				Menu       =>[],
				Context    =>"",
				Content    =>"",
				Action     =>sub{},
			},
			ViewPatient=>{
				Label      => "Theatre Lists",
				Menu       =>[],
				Context    =>"",
				Content    =>"",
				Action     =>sub{},
			},			
			Debug=>{  # when userID is specialist, and Object is not defined
				Label      => "Debug",
				Menu       => [qw/EditCode/],
				Context    => "Enter data for new patient",
				Content    => "Form to enter patient details",
				Action     => sub{},
			},
			Error=>{  # when userID is specialist, and Object is not defined
				Label      => "Edit Code",
				Menu       => [qw/Debug/],
				Context    => "Error in programs",
				Content    => sub{ return &main::dumpit($main::context) } ,
				Action     => sub{},
			},
