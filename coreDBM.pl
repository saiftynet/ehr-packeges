#!/usr/bin/env perl 
use strict;
use warnings;

 use CGI::Carp qw(fatalsToBrowser);

my $Version=0.01;
my $reqURI = $ENV{'REQUEST_URI'};
my $host = $ENV{'HTTP_HOST'};
my $docRoot = $ENV{'DOCUMENT_ROOT'};
my $visitorIP = $ENV{'REMOTE_ADDR'}; ##REMOTE_HOST in some systems;
my $rootfolder=$docRoot?"$docRoot/Traumabase":"../Traumabase";
our $ErrorMessage="";
our $SuccessMessage="";

my %WebBits=(
    Breadcrumbs  =>"Breadcrumbs not created",
    Context      =>"Context not defined",
    Menu         =>"No Menu Loaded",
    Content      =>"No content created",
    User         =>"Please login",
  );

our %in=(
	SESSIONID=> int(rand(1000000)),
	SID      => 0,
	OID      => "System",
	ACTION   => "Login"
);

if (! -d $rootfolder){#  this initialise system for first use i.ie creates teh traumabase folder and prepopolates the folder
	mkdir($rootfolder, 0755) || die "Unable to make root folder $rootfolder \n";

    my @validRoles =qw/Specialist WardStaff Referer Theatre Clinic Admin Patient/; 
    my @miscFolders=qw/Logs Lists Teams Sessions/;

    foreach my $type (@validRoles, @miscFolders){
	  if (!-d "$rootfolder/$type") {
		mkdir("$rootfolder/$type", 0755) || die "Unable to make $type folder\n";
	  }
    }
    foreach my $type (@validRoles){
    	open (my $fh,">", "$rootfolder/$type/Template") || die "unable to create user template files\n";
	    print $fh "File Profile\nUID\nUN\nPW\nROLE\nFNAME\nSNAME\nEMAIL\nTELEPHONE";
	    close $fh;
	    open ($fh,">", "$rootfolder/$type/Index") || die "unable to create user template files\n";
	    print $fh "Index file created on ".timeStamp()."\n";
	    close $fh;
    }

}

#load possible contexts
my %contexts;
unless (%contexts=do "contexts.pl"){
            warn "couldn't parse contexts: $@" if $@;
            warn "couldn't run contexts"       unless %contexts;
        }

if ($host){getParams()};
 
if ($in{ACTION} eq "Login"){
	setContext("Login");
} 
elsif (permittedContext()){
	my $target=$in{ACTION};
	if ($target){
		if ($target=~/do(.+)/){
			$contexts{$1}{Action}->(\%in);
		}
		setContext($in{ACTION});
	}
	else{  setContext("Error") }
	
}

webPageOut();

sub appendLine{
	my ($file,$line);
	open(my $fd, ">>", $file);
    print $fd $line;
    close $fd;
}

sub makeRecord{
	my $inRef=shift;
	unless ($$inRef{ROLE}  && $$inRef{OID}){
		$ErrorMessage="Error:  ROLE or ID not specified"; 
		return 0;
	}
	
	my $parentDir="$rootfolder/$$inRef{ROLE}";
	my $objectDir="$parentDir/$$inRef{OID}";
	
	if (-d $objectDir){
		$ErrorMessage="Error:  Record already exists: can not create $objectDir for $$inRef{OID}"; 
		return 0;
	}
	else{
		unless (mkdir ($objectDir, 0755)){
			$ErrorMessage="Error:  Can not create $objectDir for  $$inRef{OID}...$!";
			return 0;
			} 
		$SuccessMessage="Object Folder Created";
		appendLine("$parentDir/Index","{UID=>$in{OID}, created=>\"".timeStamp()."\", author=>\"$in{SID}\", },\n");
		if (-e "$parentDir/Template"){
			my $file="";my $path=$objectDir; my $fh; my $fopen=0; my @dirs=();;my %files=();
			open ($fh, '<',  "$parentDir/Template") || die "Unable to read template file $parentDir/Template<br>" ;
			chomp(my @lines = <$fh>);
			close $fh;
			foreach (@lines){
				if ($_=~/Folder\s+(\w+)\s*$/){
					$path=$objectDir."/".$1;
					mkdir ($path, 0755)  || die "Unable to make $path folder $!";
					$SuccessMessage.=" Folder $path created. ";
				}
				elsif ($_=~/File\s+(\w+)/){
					 $file=$1;
					 $files{$file}{path}=$path."/".$file;
					 $files{$file}{data}="** ".timeStamp(). "  Created by ". $$inRef{SID}."\n";
					 $SuccessMessage.=" $file planned in $path. ";
			    }
			    elsif ($file ne ""){
					$files{$file}{data}.="$_ = ".quotemeta($$inRef{$_})."\n";
				};
			}
			
			foreach my $d (@dirs){  mkdir ($d,  0755) };
			foreach my $file (keys %files){
				open (my $fh,">", $files{$file}{path}) || die "unable to create file ".$files{$file}{path}." $!\n";
				print $fh $files{$file}{data};
				close $fh;
			}
			
		}
		else {
			$ErrorMessage="$parentDir/Template not found<br>"
		}
		
	}
	return 1;
}

sub login{
	if(getValue($in{ROLE}."/".$in{UN},"Profile:PW") eq $in{PW}){
		$in{SID}=uc(substr($in{ROLE}, 0, 3)).$in{UN};
		delete ($in{UN});delete ($in{PW});
		$in{OID}=0;
		return 1;
		}
	else{
		return 0;
	}	
}

sub getValue{
	my ($folder,$parameter)=@_;
	$folder=$rootfolder.'/'.$folder;
	my $result='';
	if (-d $folder){
		my ($file,$item)=split(':',$parameter);
		$SuccessMessage.="Found target folder $folder<br>looking for \"$item\" in $file...";
		open (my $fh, '<',  $folder."/".$file) || die "Unable to read target file $folder/$file $!" ;
			while(<$fh>){
				chomp;
			   my ($name,$value)=split(' = ',$_,2);
			   if ($item eq $name){
				   $SuccessMessage.="Found $item... with value $value"; 
				   $result=$value }
				
			};
		close $fh;
		return $result;
	}
	else {
		$ErrorMessage.="Unable to find $folder";
	}
	
}

sub permittedContext{
	return 1;
}

sub idToPath{
	my $id=shift;
	my $prefix=substr($id, 0, 3);
	$id=~s/$prefix//;
	my %prefixes   =(   SPE  => "Specialist",
                    WAR  => "Ward",
                    REF  => "Referer",
                    THE  => "Theatres",
                    CLI  => "Clinic",
                    ADM  => "Admin",
                    PAT  => "Patient",
                    ); 
	return "$prefixes{$prefix}/$id"; 
}


# manipulate recordlines
# data is stored as lines in the file
# each record has at least a creator, a time and a value

sub addRL{
	my ($recordLine,$targetProperty)=@_;
	appendLine(toFileName($targetProperty),$recordLine)
}

sub makeRL{           # make a recordLine
	my ($name,$value)=@_;
	$value =~ s/(['"])/\\$1/g;
	$value =~ s/\r?\n/\\n/g;
	my $record="$name=>{creator=>$in{SID},timestamp=>\"".timeStamp()."\",value=>\"$value\"},\n";
	
}

sub addRLKeyValue{   # add a value to the recordline
	my ($recordLine,$key,$value);
	$value =~ s/(['"\n])/\\$1/g;
	$recordLine=~s/},\n$/$key=\"$value\"},\n/;
	return $recordLine;
}

sub getRLName{     # get the name from the record line
	my $recordLine=shift;
	return (split("=>",$recordLine,2))[0];
}

sub getRLValue{     # get the value from the record line
	my $recordLine=shift;
	my ($name,$hash)=split("=>",$recordLine,2);
	my %rec=eval $hash;
	return $rec{value}	
}

# Lists of records

sub makeTable{
	my ($objectsRefs,$headersRefs,$namesRefs);
	my $table="<table>\n".makeHeader($headersRefs);
	my @objects=@$objectsRefs;
	foreach (@objects){
		$table.=makeRow($_,$namesRefs)
	}
	$table.="</table>"
}


sub makeHeader{
	my $headersRefs=shift;
	my @headers=@$headersRefs;
	my $row="  <tr class=headerRow>";
	foreach (@headers){
		$row.="    <th>$_</th>\n";
	}
	return $row."  <tr>\n";
}

sub makeRow{
	my ($objectID,$namesRefs)=@_;
	my @names=@$namesRefs;
	my $row="  <tr class=objectRow>\n";
	$row.="    <td>$objectID</td>\n";
	foreach my $name (@names){
		$row.="    <td>".getValue($name,$objectID)."</td>\n";
	}
	return $row."  <tr>\n";
}

sub makeForm{
	my $formTemplate=shift;
	my @inputs=split(/\n/,$formTemplate);
	my $preamble="";
	my $form="";
	foreach my $inpLine (@inputs){
		$inpLine=~s/^\s+|\s+$//g;
		if ($inpLine=~/input:([^:]+):(.*)$/){
			 my($label,$name)=($1,$2);
			 my $type=($name=~/PW$/)?" type=password ":"";
			 $form.= "<tr><td align=center>$label</td><td><input style='width:100%;' $type name=$name onchange='indata[\"$name\"]=this.value'></td></tr>\n";
		}
		elsif ($inpLine=~/dropdown\(([^)]+)\):([^:]+):(.*)$/){
			my($list,$label,$name)=($1,$2,$3);
			$form.= "<tr><td align=center>$label</td>\n<td>\n<select name=$name  onchange='indata[\"$name\"]=this.value'>\n".
			        "<option value=\"\">Choose Role</option>\n";
			foreach (split(",",$list)){
				$form.="<option>$_</option>\n";
			}
			$form.="</select></td></tr>";
			
		}
		elsif ($inpLine=~/text:([^:]+):(.*)$/){
			 my($label,$name)=($1,$2);
			 $form.= "<tr><td align=center>$label</td><td><textarea style='width:100%;' name=$name onchange='indata[\"$name\"]=this.value'>enter notes</textarea></td></tr>\n";
		}
		else{
			$preamble.=$inpLine." ";
		}
		
	} 
	$preamble="<span style=\"color:green\">$SuccessMessage</span>\n$preamble<br>\n<span style=\"color:red\">$ErrorMessage</span>\n";
	 return "<table  style='width:100%;height:10px'><tr><td colspan=2>$preamble</td></tr>\n".$form.
	        "<tr><td colspan=2 align=center><span class=menulink onclick='submitForm(\"do$in{ACTION}\")'>Submit</span>
	               </td></tr>\n</table>\n";
}

# single Objects
sub timeStamp{
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=localtime(time);
    my $nice_timestamp = sprintf ( "%04d%02d%02d %02d:%02d:%02d",
                                   $year+1900,$mon+1,$mday,$hour,$min,$sec);
    return $nice_timestamp;
}

sub logLine{  # three copies of the logline are created, Root, Subject and Object
	my $logLine=shift;
	$WebBits{Content}.=$logLine;   ## remove when ebugging to main window not required
	appendLine("$rootfolder/Logs/LogFile",$logLine);
	appendLine("$rootfolder/".idToPath($in{SID})."Logs/LogFile",$logLine);
	appendLine("$rootfolder/".idToPath($in{OID})."Logs/LogFile",$logLine);
}

sub message{
	
}

sub setContext{
	my $c=shift;
	if (exists $contexts{$c}) {
		my %context=%{$contexts{$c}};
		$WebBits{Context}=$context{Context};
		makeMenu($context{Menu});
		makeContent($context{Content});
		makeUserLink();
	}
	else{
		$WebBits{Context} = "Error Target not defined";
		makeMenu( [qw/Debug EditCode/] );
		$WebBits{Content} = dumpIn();
	}
}

sub makeMenu{
	my $menuRef=shift;
	my @menu=@{$menuRef};
	$WebBits{Menu}="";
	foreach (@menu){
		$WebBits{Menu}.=makeLink($_)."<br>\n";		
	}
	
}

sub makeLink{
	my $m= shift;
	my $label=$contexts{$m}{Label};
	$label=~s/ /&nbsp;/g;
	return "<span class=menulink onclick='submitForm(\"".$m."\")'>".$label."</span>";
}

sub makeContent{
	my $c=shift;
	$WebBits{Content}=makeForm($c);	
}

sub makeUserLink{
	if ($in{SID}){
	   my $fName=getValue(idToPath($in{SID}),"Profile:FNAME");
	   if ($fName eq ''){$fName=substr($in{SID},3)};
	   $WebBits{User}="<span class=menulink onclick='submitForm(\"Profile\")'>".$fName."</span>";
   }
}

sub webPageOut{
	#print $q->header();
print <<ENDHTML;
Content-Type: text/html; charset=UTF-8

<html>
    <title>TraumaBase</title>
	<script>
	    var indata={};
		indata["SID"]="$in{SID}";
		indata["OID"]="$in{OID}";
		indata["SESSIONID"]="$in{SESSIONID}";
		
	    function submitForm(a){
	        indata["ACTION"]=a;
	         send();
	    }
	    
	    function send(){
	        var form = document.createElement("form");
		    form.method="POST";
		    form.action="../cgi-bin/coreDBM.pl";
		    var message=""
	        for(var key in indata){
		      var i=document.createElement("input");
		      i.name=key;
		      i.value=indata[key];
		      form.appendChild(i);
		      message+=key+" => "+indata[key]+"\\n"
		    }
		    document.getElementById("form").appendChild(form);
	        alert(message);
	        form.submit();
	    }
	</script>
	<style>
	table{		width:800px;		height:100%;		border:solid;		border-width:2px;		font-family:sans;		border-collapse:separate;    border-radius:10px;-moz-border-radius:10px	}
	tr{		border:solid;		border-width:1px;	}	
	td{		border:solid;		border-width:1px;	}
	.menu{		width:10em;		height:100%;		vertical-align:top;		line-height:1.8em;	}
	.main{		width:100%;		height:100%;		overflow:scroll;		vertical-align:middle;	}
	.contextbar{		background-color:whitesmoke;	}
	.context{		font-weight:bolder;		float:left;		background-color:lightgray;	}
	.user{		font-weight:bolder;		color:darkblue;		float:right;		background-color:lightgray;	}
	.bclink{		border-radius: 5px;		background: wheat;		border: 2px solid #73AD21;	}
	.bclink:hover{		background: chocolate;	}
	.menulink{		border-radius: 5px;		background: wheat;		border: 2px solid #73AD21;	}
	.menulink:hover{		background: chocolate;	}
	</style>
<body>
	<center>
<table >

<tr><td Colspan=2 class=breadcrumbbar id=breadcrumbbar>
$WebBits{Breadcrumbs}</td></tr>

<tr><td Colspan=2 class=contextbar id=contextbar>
<div class=context id=context>$WebBits{Context}</div><div class=user>$WebBits{User}</div>
</td></tr>

<tr> <td class=menu id=menu >$WebBits{Menu}</td>
<td class=main id=main > $WebBits{Content}</td></tr>
<tr><td Colspan=2 class=statusbar id=statusbar>
$in{SID} $in{OID} $in{SESSIONID} $in{ACTION}

</td></tr>
</table>
</center>
<div id="form" style="display: none;"></div>
</body>
</html>	
ENDHTML
	
}

### this gets rid of CGI or PSGI dependency
### https://www.perlmonks.org/?node_id=19154
### doesnt allow multiple values per key
### cookie handling removed
sub getParams {

	my (@form_data);

	if ($ENV{'REQUEST_METHOD'} eq "GET"){
		@form_data = split(/&/, $ENV{'QUERY_STRING'});
	}
	elsif ($ENV{'REQUEST_METHOD'} eq "POST"){
		read(STDIN, my $form_data, $ENV{'CONTENT_LENGTH'});
     	@form_data = split(/&/, $form_data);
	}
	foreach  (@form_data){
			extract($_)
	}
	
	sub extract{
		my $form=shift;
		(my $key, my $value) = split(/=/, $form);
		if ($key =~ m/\+/){
			$key =~ s/(\+)/" "/eg;
		}
		if ($value =~ m/\+/){
			$value =~ s/(\+)/" "/eg;
		}
		$key =~ s/%([\da-fA-F][\dA-Fa-f])/pack("C", hex($1))/eg;
		$value =~ s/%([\da-fA-F][\dA-Fa-f])/pack("C", hex($1))/eg;
		$in{$key} = $value;
		
	}
}

sub dumpIn{
	my $dump="Data Dump for %in<br>";
	foreach my $k (keys %in){
		$dump.=$k." = ".$in{$k}."<br>\n";		
	}
	return $dump;
}
