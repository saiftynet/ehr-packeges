package Anything;

	sub new{
		my $class  = shift;
		my %params = @_;
		
		my $UID          = autogenID($params{Type},$params{Creator});
		my $permissions  = autogenPermissions($UID);

		bless {
		    Type         => $params{'Type'},
		    Creator      => $params{'Creator'},
		    Permissions  => $permissions,     # who can do thins to this object
		    Data         => {},               # what it knows about it self
		    Actions      => {},               # things this object can do to other objects
		    UID          => $UID,  # Considered best practice to keep the superfluous comma
		}, $class;
	}
	
	sub autogenID{
	    my ($type,$creator)=@_;
	}
	
	sub autogenPermissions{   
	    my $UID=shift;
	}
	
	sub view{
	    my ($self,$viewer,$format) =@_;
	}
	
	sub addData {  # add Data to this object 
        my ($self,$actor,$data,$value)	=@_;
        
	}
	
	sub addAction{
	    my ($self,$actionName)
	    
	}
	
	sub act   { 
	    my ($self, $target, $action) =@_;
	    
	    
	}
	
	
	
__DATA__	
	
